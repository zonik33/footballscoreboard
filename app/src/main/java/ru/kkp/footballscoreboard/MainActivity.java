package ru.kkp.footballscoreboard;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Integer counter = 0, counter2 = 0;

    TextView counterView, counter2View;

    Button button, button2, button3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        counterView = findViewById(R.id.txt_counter);
        counter2View = findViewById(R.id.TextView);

        button = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);

        button.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("count", counter);
        outState.putInt("count2", counter2);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        counter = savedInstanceState.getInt("count");
        counter2 = savedInstanceState.getInt("count2");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                counter++;
                counterView.setText(counter.toString());
                break;
            case R.id.button2:
                counter2++;
                counter2View.setText(counter2.toString());
                break;
            case R.id.button3:
                counter = 0;
                counter2 = 0;
                counterView.setText(counter.toString());
                counter2View.setText(counter.toString());
                break;

        }
    }
}
